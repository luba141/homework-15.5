﻿#include <iostream>
int const N = 10;
int Array[N][N];
int main()
{
    std::string space = " ";
    int sum = 0;
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {

            Array[i][j] = i + j;
            if(i == (N % 5))
                sum += Array[i][j];
            //чтобы красиво вывести заданный массив
            if (Array[i][j] < 10)
                space = "  ";
            else
                space = " ";

            std::cout << Array[i][j] << space;

        }

        std::cout << '\n';
    }
    std::cout << "index: " << N % 5 << " sum: " << sum;
}